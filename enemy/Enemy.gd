extends RigidBody2D

signal shoot

export (PackedScene) var Amunition
export (int) var engine_power = 150
export (int) var spin_power = 3200
export (int) var health = 3
export (int) var speed = 150

var follow
var target = null
var move_to = null

func _ready():
	$Sprite.frame = randi() % 3
	var path = $EnemyPaths.get_children()[randi() % $EnemyPaths.get_child_count()]
	follow = PathFollow2D.new()
	path.add_child(follow)
	follow.loop = false
	
func _on_AnimationPlayer_animation_finished(anim_name):
	queue_free()

func _process(delta):
	follow.offset += speed * delta
	move_to = follow.global_position
	if follow.unit_offset > 1:
		queue_free()
	navigate_to(move_to)
	
func navigate_to(move_target):
	var moving = Vector2(1,0).rotated(rotation)
	#var moving = (-linear_velocity).normalized()
	var target = (move_target - global_position).normalized()
	var rotation_dir = 0
	var dp = moving.dot(target)
	if dp > 0:
		rotation_dir -= 1
	elif dp < 0:
		rotation_dir += 1
	
	set_applied_force(Vector2(engine_power, 0).rotated(rotation))
	set_applied_torque(spin_power * rotation_dir)

	