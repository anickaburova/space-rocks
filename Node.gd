extends Node

var level = 0
var score = 0
var playing = false

export (PackedScene) var Rock
var screensize

func _ready():
	randomize()
	screensize = get_viewport().get_visible_rect().size
	$Player.screensize = screensize
	$Player.position = screensize / 2
	#for i in range(3):
		#spawn_rock(3)

func _on_Player_shoot(ammunition, pos, dir):
	var p = ammunition.instance()
	p.start(pos, dir)
	add_child(p)

func spawn_rock(size, pos = null, vel = null):
	if !pos:
		$RockPath/RockSpawn.set_offset(randi())
		pos = $RockPath/RockSpawn.position
	if !vel:
		vel = Vector2(1,0).rotated(rand_range(0,2*PI)) * rand_range(100,150)
	var r = Rock.instance()
	r.screensize = screensize
	r.start(pos, vel, size)
	$Rocks.add_child(r)
	r.connect('exploded', self, '_on_Rock_exploded')
	
func _on_Rock_exploded(size, radius, position, explosion_position, vel):
	score += 1
	$HUD.update_score(score)

	if size <= 1:
		return
	for offset in [-1,1]:
		var dir = (position - $Player.global_position).normalized().tangent() * offset
		var newpos = position + dir * radius
		var newvel = dir * vel.length() * rand_range(0.9,1.1)
		spawn_rock(size - 1, newpos, newvel)
		
func clear_rocks():
	for rock in $Rocks.get_children():
		rock.queue_free()
		
func new_game():
	clear_rocks()
	level = 0
	score = 0
	$HUD.update_score(score)
	$Player.start()
	$HUD.show_message('Get Ready!')
	yield($HUD/MessageTimer, 'timeout')
	playing = true
	new_level()
	
func new_level():
	level += 1
	$HUD.show_message('Wave %s' % level)
	for i in range(level):
		spawn_rock(3)
		
func _process(delta):
	if playing and $Rocks.get_child_count() == 0:
		new_level()
		
func game_over():
	playing = false
	$HUD.game_over()

func _on_Player_lives_changed(lives):
	$HUD.update_lives(lives)
	
func _input(event):
	if playing and  event.is_action_pressed('pause'):
		get_tree().paused = not get_tree().paused
		if get_tree().paused:
			$HUD/MessageLabel.text = 'Paused'
			$HUD/MessageLabel.show()
		else:
			$HUD/MessageLabel.text = ''
			$HUD/MessageLabel.hide()
