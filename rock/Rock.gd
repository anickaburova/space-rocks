extends RigidBody2D

signal exploded

var screensize = Vector2()
var size
var radius
var scale_factor = 0.2

func explode(explosion_position):
	layers = 0
	$Sprite.hide()
	$Explosion/AnimationPlayer.play('explosion')
	var lv = linear_velocity
	linear_velocity = Vector2()
	angular_velocity = 0
	yield($Explosion/AnimationPlayer, 'animation_finished')
	emit_signal('exploded', size, radius, position, explosion_position, lv)
	queue_free()

func start(pos, vel, _size):
	$Explosion.scale = Vector2(0.75, 0.75) * _size
	position = pos
	size = _size
	mass = 1.5 * size
	$Sprite.scale = Vector2(1,1) * scale_factor * size
	radius = int($Sprite.texture.get_size().x / 2 * scale_factor * size)
	var shape = CircleShape2D.new()
	shape.radius = radius
	$CollisionShape2D.shape = shape
	linear_velocity = vel
	angular_velocity = rand_range(-1.5, 1.5)
	
func _integrate_forces(pstate):
	var xform = pstate.get_transform()
	if xform.origin.x > screensize.x + radius:
		xform.origin.x = -radius
	if xform.origin.y > screensize.y + radius:
		xform.origin.y = -radius
	if xform.origin.x < -radius :
		xform.origin.x = screensize.x + radius
	if xform.origin.y < -radius:
		xform.origin.y = screensize.y + radius
	pstate.set_transform(xform)
