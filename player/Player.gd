extends RigidBody2D

signal shoot
signal lives_changed
signal dead

export (PackedScene) var Ammunition
export (float) var fire_rate
var can_shoot = true

export (int) var engine_power
export (int) var spin_power

var thrust = Vector2()
var rotation_dir = 0

var lives = 0 setget set_lives

onready var screensize = get_viewport().get_visible_rect().size

enum {
		INIT,
		ALIVE,
		INVULNERABLE,
		DEAD,
	}
var state = null
	
func _ready():
	$Sprite.hide()
	$GunTimer.wait_time = fire_rate
	change_state(INIT)
	
func change_state(new_state):
	match new_state:
		INIT:
			$CollisionShape2D.disabled = true
		ALIVE:
			$CollisionShape2D.disabled = false
		INVULNERABLE:
			$CollisionShape2D.disabled = true
			$InvlTimer.start()
		DEAD:
			$CollisionShape2D.disabled = true
			$Sprite.hide()
			linear_velocity = Vector2()
			emit_signal('dead')
	state = new_state

func _process(delta):
	get_input()
	
func get_input():
	thrust = Vector2()
	if state in [DEAD, INIT]:
		return
	if Input.is_action_pressed('thrust'):
		thrust = Vector2(engine_power, 0)
	
	rotation_dir = 0
	if Input.is_action_pressed('rotate_right'):
		rotation_dir += 1
	if Input.is_action_pressed('rotate_left'):
		rotation_dir -= 1
	
	if can_shoot and Input.is_action_pressed('shoot'):
		shoot()
		
func _integrate_forces(pstate):
	set_applied_force(thrust.rotated(rotation))
	set_applied_torque(spin_power * rotation_dir)
	var xform = pstate.get_transform()
	if xform.origin.x > screensize.x:
		xform.origin.x = 0
	if xform.origin.y > screensize.y:
		xform.origin.y = 0
	if xform.origin.x < 0:
		xform.origin.x = screensize.x
	if xform.origin.y < 0:
		xform.origin.y = screensize.y
	pstate.set_transform(xform)
		

func _on_GunTimer_timeout():
	can_shoot = true

func shoot():
	if state == INVULNERABLE:
		return
	emit_signal('shoot', Ammunition, $Muzzle.global_position, rotation)
	can_shoot = false
	$GunTimer.start()
	
func start():
	$Sprite.show()
	self.lives = 3
	change_state(ALIVE)
	
func set_lives(value):
	lives = value
	emit_signal('lives_changed', lives)
	

func _on_InvlTimer_timeout():
	change_state(ALIVE)

func _on_AnimationPlayer_animation_finished(anim_name):
	$Explosion.hide()


func _on_Player_body_entered(body):
	if body.is_in_group('rocks'):
		body.explode(global_position)
		$Explosion.show()
		$Explosion/AnimationPlayer.play('explosion')
		self.lives -= 1
		if lives <= 0:
			change_state(DEAD)
		else:
			change_state(INVULNERABLE)
