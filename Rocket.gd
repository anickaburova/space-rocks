extends RigidBody2D

export (int) var speed

var velocity = Vector2()

func start(pos, dir):
	position = pos
	rotation = dir
	velocity = Vector2(speed, 0)
	
func _integrate_forces(pstate):
	set_applied_force(velocity.rotated(rotation))
	#set_applied_torque(spin_power * rotation_dir)


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()


func _on_Trigger_body_entered(body):
	if body.is_in_group('rocks'):
		body.explode(global_position)
		queue_free()


func _on_Rocket_body_entered(body):
	pass # replace with function body
